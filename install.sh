#!/bin/sh

# This is the main installation script

cd bedrock
composer install && npm install
sudo chmod -R 777 web/app/uploads
cp .env.example .env
cd ..

cd laravel
composer install && npm install

sudo chmod -R 777 storage
sudo chmod -R 777 bootstrap/cache
cp .env.example .env
