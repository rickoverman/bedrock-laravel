<?php

require_once base_path(). '/../bedrock/web/wp/wp-load.php';
get_header();

?>

<article id="post-98" class="post-98 post type-post status-publish format-standard hentry category-uncategorized">

	<header class="entry-header">
		<h2 class="entry-title"><a href="http://wpl5.localhost/index.php/2016/07/21/this-is-a-test-post/" rel="bookmark">This is a laravel route!</a></h2>	</header><!-- .entry-header -->

	<div class="entry-content">
		<p><img src="http://www.pixelindustries.com/cms_img/laravellogowhite.png" /></p>
	</div><!-- .entry-content -->


	<footer class="entry-footer">
		<span class="posted-on"><span class="screen-reader-text">Posted on </span><a href="http://wpl5.localhost/index.php/2016/07/21/this-is-a-test-post/" rel="bookmark"><time class="entry-date published" datetime="2016-07-21T01:54:29+00:00">July 21, 2016</time><time class="updated" datetime="2016-07-21T03:58:49+00:00">July 21, 2016</time></a></span><span class="cat-links"><span class="screen-reader-text">Categories </span><a href="http://wpl5.localhost/index.php/category/uncategorized/" rel="category tag">Uncategorized</a></span><span class="comments-link"><a href="http://wpl5.localhost/index.php/2016/07/21/this-is-a-test-post/#respond">Leave a comment<span class="screen-reader-text"> on This is a test post</span></a></span>		<span class="edit-link"><a class="post-edit-link" href="http://wpl5.localhost/wp/wp-admin/post.php?post=98&amp;action=edit">Edit</a></span>	</footer><!-- .entry-footer -->

</article>

<?php

get_footer();

?>
